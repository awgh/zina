<?php
if (!function_exists('str_getcsv')) {
    function str_getcsv($input, $delimiter = ",", $enclosure = '"', $escape = "\\") {
        $MBs = 1 * 1024 * 1024;
        $fp = fopen("php://temp/maxmemory:$MBs", 'r+');
        fputs($fp, $input);
        rewind($fp);

        $data = fgetcsv($fp, 1000, $delimiter, $enclosure); //  $escape only got added in 5.3.0

        fclose($fp);
        return $data;
    }
}
if (!function_exists('fetchAllBetween')) {
    function fetchAllBetween($needle1,$needle2,$haystack,$include=false){
        
        $matches = array();
        
        $exp = "|{$needle1}(.*){$needle2}|U";
        
        preg_match_all($exp,$haystack,$matches);
        
        $i = ($include == true) ? 0 : 1 ;
        
        return $matches[$i];
        
    }
}

function zina_extras_images_google($artist, $album = null, $num = 3, $size = 'huge') {
	if (empty($artist)) return false;

	$url = 'http://images.google.com/images?q='.urlencode($artist).((!empty($album)) ? '+'.urlencode($album) : '').
		'&gbv=2&hl=en&safe=on&sa=G';

	$result = false;
	if ($html = file_get_contents($url)) {

        $imgurls = fetchAllBetween('imgurl=','&',$html);		        
        
        for($i=0; $i < $num && $i < count($imgurls); $i++) {
            $result[] = array(
                'image_url' => $imgurls[$i],
                'source_url' => $url,
                #'size' => -1,
                'thumbnail_url' => $imgurls[$i],
                );
        }
    }

	return $result;
}

?>
